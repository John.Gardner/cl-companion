import tkinter as tk
from tkinter import ttk
from utilities import my_driver, login, expand_learn_blocks, get_practice_problems, pp_link_fixer, get_students

def main_function():
    #Grabs these values from the GUI inputs
    user_email = email_string.get()
    user_password = password_string.get()
    user_url = learn_url_string.get()
    

    # Initializes the webdriver
    driver = my_driver()
    
    # Logs the user into Learn
    login(driver, user_email, user_password, user_url)
    # Makes sure all the Learn modules have loaded
    expand_learn_blocks(driver)
    # Gets the Practice Problem links from Learn
    pp_links = get_practice_problems(driver)
    # Fixes some issues that may exist with the links
    fixed_links = pp_link_fixer(pp_links)
    # Gets names of students who haven't opened the
    # practice problems
    get_students(driver, fixed_links)


# create main window
window = tk.Tk()
window.title('CL Companion - Galvanize')
window.geometry('360x640')

# title
title_label = ttk.Label(
    master = window,
    text = 'Welcome to CL Companion!',
    font = 'Calibri 24 bold'
)
title_label.pack()

# input frames
email_frame = ttk.Frame(master = window)
password_frame = ttk.Frame(master = window)
learn_url_frame = ttk.Frame(master = window)

# Email input
email_string = tk.StringVar()
email_input = ttk.Entry(
    master = email_frame,
    textvariable = email_string
)
email_label = ttk.Label(
    master = email_frame,
    text = 'email',
    font = 'Calibri 18'
)

# Password input
password_string = tk.StringVar()
password_input = ttk.Entry(
    master = password_frame,
    show = "*",
    textvariable = password_string
)
password_label = ttk.Label(
    master = password_frame,
    text = 'password',
    font = 'Calibri 18'
)

# Learn url input
learn_url_string = tk.StringVar()
learn_url_input = ttk.Entry(
    master = learn_url_frame,
    textvariable = learn_url_string
)
learn_url_label = ttk.Label(
    master = learn_url_frame,
    text = 'Learn url',
    font = 'Calibri 18'
)

#  Submit button
button = ttk.Button(
    master = window,
    text = 'SUBMIT',
    command = main_function
)

# input packing
email_label.pack(side = 'left')
email_input.pack(side = 'left')
email_frame.pack()
password_label.pack(side = 'left')
password_input.pack(side = 'left')
password_frame.pack()
learn_url_label.pack(side = 'left')
learn_url_input.pack(side = 'left')
learn_url_frame.pack()
button.pack()



# run
window.mainloop()