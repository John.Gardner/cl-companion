# CL Companion

This is the project I worked on for the 3 days I had in development as a SEIR.
It's an app to help Cohort Leads see which students have been doing the Practice Problems every day. 
I didn't get it finished, but it's fairly functional for what's there.
I built it using Selenium, BeautifulSoup, and Tkinter.

If anyone for whatever reason decides they want to pick it up where I left off, I left comments on what everything does. If you have any questions, feel free to shoot me an email: john.gardner823@gmail.com