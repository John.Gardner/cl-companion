import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.service import Service as ChromeService
from bs4 import BeautifulSoup


def my_driver():
    options = webdriver.ChromeOptions()
    service = ChromeService(executable_path='./driver/chromedriver')
    driver = webdriver.Chrome(service=service, options=options)
    return driver


def login(driver, email, password, url):
    # load Learn url and login
    driver.get(url)
    driver.find_element(By.ID, 'user_email').send_keys(email)
    driver.find_element(By.ID, 'user_password').send_keys(password)
    driver.find_element(By.NAME, 'commit').click()


def expand_learn_blocks(driver):
    # scroll through each "Week" section and click the "expand" arrow
    curriculum = driver.find_element(By.CLASS_NAME, 'cohort-curriculum')
    blocks = curriculum.find_elements(By.CLASS_NAME, 'block-container')
    spans = []
    for item in blocks:
        spans.append(item.find_element(By.TAG_NAME, 'span'))
    for item in spans:
        item.click()
        time.sleep(0.1)
        item.click()
    driver.find_element(By.TAG_NAME, 'body').send_keys(Keys.END)
    time.sleep(3)
    

def get_practice_problems(driver):
    html = driver.page_source
    soup = BeautifulSoup(html, "html.parser")
    
    """
    I was trying to modify how I got the practice problems links
    to include the Week Number
    """
    # all_divs = soup.body.main.find_all('div')
    # learn_modules = None
    # for item in all_divs:
    #     try:
    #         if item['data-react-class']:
    #             learn_modules = item
    #     except:
    #         continue
    # print(learn_modules)
    
    practice_problem_links = []
    
    all_links = soup.find_all('a')
    for link in all_links:
        try:
            link_title = link.div['title']
            link_href = link['href']
            if '☑️' in link_title:
                if 'Practice Problems' in link_title or "Knowledge Check" in link_title:
                    practice_problem_links.append(link['href'])
        except:
            continue
    return practice_problem_links


def pp_link_fixer(list_of_urls):
    """
    Sometimes the urls scraped from Learn
    have the wrong file until the link is
    clicked on. This resolves that most of
    the time.
    """
    fixed_urls = []
    practice_problem_urls = []
    for url in list_of_urls:
        split_url = url.split("/")
        file_name = split_url[-1]
        fixed_url_segments = split_url[:-1]
        
        if "-answers" in file_name:
            new_file_name = file_name.replace('-answers', '')
            if "24-" in new_file_name:
                new_file_name = new_file_name.replace('24-', '25-')
                fixed_url_segments.append(new_file_name)
                new_url = '/'.join(fixed_url_segments)
                fixed_urls.append(new_url)
        else:
            fixed_urls.append(url)
            
    for item in fixed_urls:
        prefix = 'https://learn-2.galvanize.com'
        final_url = prefix + item
        practice_problem_urls.append(final_url)
    practice_problem_urls.sort(reverse=True)
    return practice_problem_urls


def get_students(driver, list_of_urls):
    """
    For each Practice Problem url, returns
    a list of students who haven't opened 
    the practice problems. 
    
    Returns a dictionary with each "key"
    being a practice problem link and each
    "value" being a list of students who
    haven't opened it.
    """
    did_not_open = {}
    all_urls = list_of_urls
    for url in all_urls:
        try:
            results = []
            driver.get(url)
            time.sleep(4)
            html = driver.page_source
            soup = BeautifulSoup(html, "html.parser")
            student_table = soup.tbody
            students = student_table.find_all('tr')
            for student in students:
                student_data = student.find_all('td')
                student_name = student_data[0].span['data-tip']
                student_submission = student_data[2].a
                if student_submission == None:
                    results.append(student_name)
            did_not_open[f"{url}"] = results
        except AttributeError:
            did_not_open[f"{url}"] = "could not open link :("
    # print(did_not_open)
    return did_not_open